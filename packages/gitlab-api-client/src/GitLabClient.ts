import { joinPaths } from '@gitlab/utils-path';
import { GraphQLClient } from 'graphql-request';
import { gitlab } from './types';
import {
  createProjectBranchMutation,
  CreateProjectBranchResult,
  CreateProjectBranchVariables,
  getMergeRequestDiffStatsQuery,
  GetMergeRequestDiffStatsResult,
  GetMergeRequestDiffStatsVariables,
  getProjectUserPermissionsQuery,
  getProjectUserPermissionsResult,
  getProjectUserPermissionsVariables,
  searchProjectBranchesQuery,
  SearchProjectBranchesResult,
  SearchProjectBranchesVariables,
} from './graphql';
import { createResponseError } from './createResponseError';

const withParams = (baseUrl: string, params: Record<string, string>) => {
  const paramEntries = Object.entries(params);

  if (!paramEntries.length) {
    return baseUrl;
  }

  const url = new URL(baseUrl);

  paramEntries.forEach(([key, value]) => {
    url.searchParams.append(key, value);
  });

  return url.toString();
};
export interface IGitLabClientConfig {
  baseUrl: string;
  authToken: string;
  httpHeaders?: Record<string, string>;
}

export class GitLabClient {
  readonly #baseUrl: string;

  readonly #httpHeaders: Record<string, string>;

  readonly #graphqlClient: GraphQLClient;

  constructor(config: IGitLabClientConfig) {
    this.#baseUrl = config.baseUrl;
    this.#httpHeaders = {
      ...(config.httpHeaders || {}),
      ...(config.authToken ? { 'PRIVATE-TOKEN': config.authToken } : {}),
    };

    const graphqlUrl = joinPaths(this.#baseUrl, 'api', 'graphql');
    this.#graphqlClient = new GraphQLClient(graphqlUrl, {
      headers: this.#httpHeaders,
    });
  }

  async fetchProjectUserPermissions(projectPath: string) {
    const result = await this.#graphqlClient.request<
      getProjectUserPermissionsResult,
      getProjectUserPermissionsVariables
    >(getProjectUserPermissionsQuery, {
      projectPath,
    });

    return result.project.userPermissions;
  }

  async fetchProjectBranches(params: SearchProjectBranchesVariables) {
    const result = await this.#graphqlClient.request<
      SearchProjectBranchesResult,
      SearchProjectBranchesVariables
    >(searchProjectBranchesQuery, params);

    return result.project.repository.branchNames || [];
  }

  async createProjectBranch(params: CreateProjectBranchVariables) {
    const result = await this.#graphqlClient.request<
      CreateProjectBranchResult,
      CreateProjectBranchVariables
    >(createProjectBranchMutation, params);

    return result.createBranch;
  }

  async fetchMergeRequestDiffStats({ mergeRequestId }: { mergeRequestId: string }) {
    const gid = `gid://gitlab/MergeRequest/${mergeRequestId}`;

    const result = await this.#graphqlClient.request<
      GetMergeRequestDiffStatsResult,
      GetMergeRequestDiffStatsVariables
    >(getMergeRequestDiffStatsQuery, { gid });

    return result.mergeRequest.diffStats;
  }

  fetchProject(projectId: string): Promise<gitlab.Project> {
    const url = this.#buildApiUrl('projects', projectId);

    return this.#fetchGetJson(url);
  }

  fetchMergeRequest(projectId: string, mrId: string): Promise<gitlab.MergeRequest> {
    const url = this.#buildApiUrl('projects', projectId, 'merge_requests', mrId);

    return this.#fetchGetJson(url);
  }

  fetchProjectBranch(projectId: string, branchName: string): Promise<gitlab.Branch> {
    const url = this.#buildApiUrl('projects', projectId, 'repository', 'branches', branchName);

    return this.#fetchGetJson(url);
  }

  fetchTree(projectId: string, ref: string): Promise<gitlab.RepositoryTreeItem[]> {
    const url = this.#buildApiUrl('projects', projectId, 'repository', 'tree');

    const params = {
      ref,
      recursive: 'true',
      pagination: 'none',
    };
    return this.#fetchGetJson(url, params);
  }

  commit(projectId: string, payload: gitlab.CommitPayload): Promise<gitlab.Commit> {
    const url = this.#buildApiUrl('projects', projectId, 'repository', 'commits');

    return this.#fetchPostJson(url, payload);
  }

  async fetchFileRaw(projectId: string, ref: string, path: string): Promise<ArrayBuffer> {
    const url = this.#buildApiUrl('projects', projectId, 'repository', 'files', path, 'raw');

    return this.#fetchGetBuffer(url, { ref });
  }

  #buildApiUrl(...parts: string[]) {
    return joinPaths(this.#baseUrl, 'api', 'v4', ...parts.map(encodeURIComponent));
  }

  async #fetchGetResponse(url: string, params: Record<string, string> = {}): Promise<Response> {
    const response = await fetch(withParams(url, params), {
      method: 'GET',
      headers: this.#httpHeaders,
    });

    if (!response.ok) {
      throw await createResponseError(response);
    }

    return response;
  }

  async #fetchPostJson<TResponse, TBody>(url: string, body?: TBody): Promise<TResponse> {
    const response = await fetch(url, {
      method: 'POST',
      body: body ? JSON.stringify(body) : undefined,
      headers: {
        ...this.#httpHeaders,
        'Content-Type': 'application/json',
      },
    });

    if (!response.ok) {
      throw await createResponseError(response);
    }

    return <Promise<TResponse>>response.json();
  }

  async #fetchGetJson<T>(url: string, params: Record<string, string> = {}): Promise<T> {
    const response = await this.#fetchGetResponse(url, params);

    return (await response.json()) as T;
  }

  async #fetchGetBuffer(url: string, params: Record<string, string> = {}): Promise<ArrayBuffer> {
    const response = await this.#fetchGetResponse(url, params);

    return response.arrayBuffer();
  }
}
