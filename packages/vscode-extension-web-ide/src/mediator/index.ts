import * as vscode from 'vscode';
import {
  COMMAND_START,
  COMMAND_FETCH_FILE_RAW,
  COMMAND_READY,
  COMMAND_START_REMOTE,
  COMMAND_COMMIT,
  COMMAND_PREVENT_UNLOAD,
  COMMAND_SET_HREF,
  COMMAND_FETCH_MERGE_REQUEST_DIFF_STATS,
  COMMAND_FETCH_PROJECT_BRANCHES,
  COMMAND_CREATE_PROJECT_BRANCH,
} from '@gitlab/vscode-mediator-commands';
import type {
  StartCommandResponse,
  VSCodeBuffer,
  StartRemoteMessageParams,
  PreventUnloadMessageParams,
  FetchMergeRequestDiffStatsParams,
  FetchMergeRequestDiffStatsResponse,
  FetchProjectBranchesParams,
  FetchProjectBranchesResponse,
  CreateProjectBranchParams,
  CreateProjectBranchResponse,
  GitLabCommitPayload,
  StartCommandOptions,
} from '@gitlab/vscode-mediator-commands';

export const start = (options: StartCommandOptions = {}): Thenable<StartCommandResponse> =>
  vscode.commands.executeCommand(COMMAND_START, options);

export const fetchFileRaw = (ref: string, path: string): Thenable<VSCodeBuffer> =>
  vscode.commands.executeCommand(COMMAND_FETCH_FILE_RAW, ref, path);

export const fetchMergeRequestDiffStats = (
  params: FetchMergeRequestDiffStatsParams,
): Thenable<FetchMergeRequestDiffStatsResponse> =>
  vscode.commands.executeCommand(COMMAND_FETCH_MERGE_REQUEST_DIFF_STATS, params);

export const fetchProjectBranches = (
  params: FetchProjectBranchesParams,
): Thenable<FetchProjectBranchesResponse> =>
  vscode.commands.executeCommand(COMMAND_FETCH_PROJECT_BRANCHES, params);

export const createProjectBranch = (
  params: CreateProjectBranchParams,
): Thenable<CreateProjectBranchResponse> =>
  vscode.commands.executeCommand(COMMAND_CREATE_PROJECT_BRANCH, params);

export const ready = () => vscode.commands.executeCommand(COMMAND_READY);

export const startRemote = (params: StartRemoteMessageParams) =>
  vscode.commands.executeCommand(COMMAND_START_REMOTE, params);

export const commit = (payload: GitLabCommitPayload) =>
  vscode.commands.executeCommand(COMMAND_COMMIT, payload);

export const preventUnload = (params: PreventUnloadMessageParams) =>
  vscode.commands.executeCommand(COMMAND_PREVENT_UNLOAD, params);

export const setHref = (href: string) => vscode.commands.executeCommand(COMMAND_SET_HREF, href);
