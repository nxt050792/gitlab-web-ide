import { DefaultGitLabClient } from '@gitlab/gitlab-api-client';
import { ApiRequest } from '@gitlab/web-ide-interop';

type ApiRequestCommand<T> = (request: ApiRequest<T>) => Promise<T>;

export const commandFactory =
  <T>(client: DefaultGitLabClient): ApiRequestCommand<T> =>
  async request =>
    client.fetchFromApi(request);
